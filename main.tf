resource "google_service_account" "service_account" {
  account_id   = "${var.name}-sa-${var.env}"
  display_name = "${var.name}-sa-${var.env}"
}